package pe.com.enviarcorreo.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesInternos {
	
	@Value("${spring.mail.username}")
	public String correoCoorporativo;
	
	@Value("${spring.mail.password}")
	public String claveCorreoCoorporativo;
	
	@Value("${asunto.value}")
	public String asuntoCorreo;
}
