package pe.com.enviarcorreo.util;

public class Constantes {
	
	public static String CODIGO_OK = "0";
	public static String CODIGO_ERROR_GENERADO = "-1";
	public static String CODIGO_EXITOSO = "0";
	public static String MENSAJE_EXITOSO = "Se envio el correo correctamente!";
	
	public static String CODIGO_NO_EXITOSO = "1";
	public static String MENSAJE_NO_EXITOSO = "No se envio el correo correctamente!";

}
