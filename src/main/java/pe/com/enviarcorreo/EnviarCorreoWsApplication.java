package pe.com.enviarcorreo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnviarCorreoWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnviarCorreoWsApplication.class, args);
	}

}
