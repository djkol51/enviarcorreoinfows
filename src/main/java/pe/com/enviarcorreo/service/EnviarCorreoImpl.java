package pe.com.enviarcorreo.service;

import java.io.Serializable;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import pe.com.enviarcorreo.bean.CorreoBean;
import pe.com.enviarcorreo.util.MyPasswordAuthenticator;
import pe.com.enviarcorreo.util.PropertiesInternos;

@Service(value = "enviarCorreo")
public class EnviarCorreoImpl implements EnviarCorreo, Serializable {

	@Autowired
	private PropertiesInternos propertiesInternos;

	@Autowired
	private JavaMailSender mailSender;

	private static final long serialVersionUID = 1L;

	@Override
	public int envioCorreo(CorreoBean bean) {

		Properties props = new Properties();

		String correoOrigen = propertiesInternos.correoCoorporativo;
		String clave = propertiesInternos.claveCorreoCoorporativo;
		String subject = propertiesInternos.asuntoCorreo;

		System.out.println("correoOrigen " + correoOrigen);

		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		int resultado = 0;

		Session session = null;
		session = Session.getDefaultInstance(props, null);
		session.setDebug(true);

		session = Session.getInstance(props, new MyPasswordAuthenticator(correoOrigen, clave));

		try {

			MimeMessage message = new MimeMessage(session);

			message.setFrom(new InternetAddress(correoOrigen));

			InternetAddress mail_to = new InternetAddress(bean.getCorreoDestino().trim());

			message.addRecipient(Message.RecipientType.TO, mail_to);

			message.setSubject(subject);

			MimeMultipart multipart = new MimeMultipart();

			BodyPart bodyPart = new MimeBodyPart();

			String divTable = "<!DOCTYPE html><html><body><div style="
					+ "width:750px; height:450px; position:relative;background-repeat: no-repeat;text-align:justify "
					+ ">";

			String str1 = "<tr><td></td></tr>";
			String str2 = "<tr><td><b>Estimado :  " + bean.getNombresCompletos().trim() + "</b></td></tr>";
			String salto = "<tr><td><br/></td></tr>";

			String str3 = "<tr><td style='text-align: justify'><p align='justify'>La siguiente comunicación tiene como finalidad en avisarle que su cuenta se ha registrado con exito! <b></b></p></td></tr>";
			String salto1 = "<tr><td><br/></td></tr>";

			String str4 = "<tr><td>Atentamente. <br/></td></tr>";
			String salto2 = "<tr><td><br/></td></tr>";
			String str5 = "<tr><td>El Equipo de Glovo<br/></td></tr>";

			String divFinal = "</table></body></html></div>";

			String cuerpo = divTable + str1 + str2 + salto + str3 + salto1 + str4 + salto2 + str5 + divFinal;

			System.out.println("cuerpo : \n" + cuerpo);

			bodyPart.setHeader("Content-Type", "charset=UTF-8");

			bodyPart.setContent(cuerpo, "text/html;charset=UTF-8");

			multipart.addBodyPart(bodyPart);

			message.setContent(multipart, "UTF-8");

			Transport.send(message);

			resultado = 1;

		} catch (Exception e) {
			e.printStackTrace();
			resultado = 0;
		}

		return resultado;
	}

	@Override
	public int enviarCorreo(CorreoBean bean) {

		int resultado = 0;

		String subject = propertiesInternos.asuntoCorreo;

		try {

			MimeMessage message = mailSender.createMimeMessage();

//			MimeMessageHelper helper = new MimeMessageHelper(message);

			InternetAddress mail_to = new InternetAddress(bean.getCorreoDestino().trim());
			
			message.addRecipient(Message.RecipientType.TO, mail_to);

			message.setSubject(subject);
			
			MimeMultipart multipart = new MimeMultipart();
			
			BodyPart bodyPart = new MimeBodyPart();

//			helper.setTo(mail_to);

//			helper.setSubject(subject);

			String divTable = "<!DOCTYPE html><html><body><div style="
					+ "width:750px; height:450px; position:relative;background-repeat: no-repeat;text-align:justify "
					+ ">";

			String str1 = "<tr><td></td></tr>";
			String str2 = "<tr><td><b>Estimado :  " + bean.getNombresCompletos().trim() + "</b></td></tr>";
			String salto = "<tr><td><br/></td></tr>";

			String str3 = "<tr><td style='text-align: justify'><p align='justify'>El correo tiene como finalidad avisarle que su cuenta se ha registrado con exito! <b></b></p></td></tr>";
			String salto1 = "<tr><td><br/></td></tr>";

			String str4 = "<tr><td>Atentamente. <br/></td></tr>";
			String salto2 = "<tr><td><br/></td></tr>";
			String str5 = "<tr><td>El Equipo de Glovo<br/></td></tr>";
			
			String tablaBanda ="<tr><td><table width='750px' height='45px' border='0'>"+"<td style='background-color:#0080FF; font-size: 12px; color: #FFFFFF; text-align: center'>";
			String banda = "<a href='#'><font color='yellow'></a></table>";

			String divFinal = "</table></body></html></div>";

			String cuerpo = divTable + str1 + str2 + salto + str3 + salto1 + str4 + salto2 + str5 + 
					tablaBanda + banda +divFinal;

//			helper.setText(cuerpo);
			
			bodyPart.setHeader("Content-Type", "charset=UTF-8");

			bodyPart.setContent(cuerpo, "text/html;charset=UTF-8");

			multipart.addBodyPart(bodyPart);
			
			message.setContent(multipart, "UTF-8");

			mailSender.send(message);
			
			resultado = 1;

		} catch (Exception e) {
			e.printStackTrace();
			resultado = 0;
		}

		return resultado;
	}

}
