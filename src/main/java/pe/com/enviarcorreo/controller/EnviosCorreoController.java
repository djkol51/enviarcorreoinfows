package pe.com.enviarcorreo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.enviarcorreo.bean.CorreoBean;
import pe.com.enviarcorreo.response.GenericoResponse;
import pe.com.enviarcorreo.service.EnviarCorreo;
import pe.com.enviarcorreo.util.Constantes;

@RestController
@RequestMapping("/correo")
public class EnviosCorreoController {

	@Autowired
	private EnviarCorreo enviarCorreo;

	@PostMapping(value = "enviar", headers = "accept=Application/json")
	public ResponseEntity<GenericoResponse> envioCorreoGlovo(
			@RequestBody CorreoBean req) {

		ResponseEntity<GenericoResponse> entity = null;
		
		GenericoResponse response = new GenericoResponse();

		try {
			
			int respuesta = enviarCorreo.enviarCorreo(req);
			
			if(respuesta == 1) {
				response.setCodigoRespuesta(Constantes.CODIGO_EXITOSO);
				response.setMensajeRespuesta(Constantes.MENSAJE_EXITOSO);
				entity = new ResponseEntity<GenericoResponse>(response,HttpStatus.OK);
			}else {
				response.setCodigoRespuesta(Constantes.CODIGO_NO_EXITOSO);
				response.setMensajeRespuesta(Constantes.MENSAJE_NO_EXITOSO);
				entity = new ResponseEntity<GenericoResponse>(response
						,HttpStatus.NOT_FOUND);
			}
			

		} catch (Exception e) {
			e.printStackTrace();
			response.setCodigoRespuesta(Constantes.CODIGO_ERROR_GENERADO);
			response.setMensajeRespuesta("Error en : "+ e);
			entity = new ResponseEntity<GenericoResponse>(response
					,HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return entity;

	}

}
